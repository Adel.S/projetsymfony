<?php

namespace App\Repository;

use App\Entity\TableauParticipant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TableauParticipant|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableauParticipant|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableauParticipant[]    findAll()
 * @method TableauParticipant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableauParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableauParticipant::class);
    }

    // /**
    //  * @return TableauParticipant[] Returns an array of TableauParticipant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TableauParticipant
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

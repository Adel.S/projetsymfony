<?php

namespace App\Entity;



class Participant
{


    private $Nom;

    private $montantAvance;

    private $montantAPAyer = 0;

    private $montantARecevoir = 0;


    public function getNom()
    {
        return  $this->Nom;
    }

    public function setNom(string $Nom)
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getMontantAvance()
    {
        return $this->montantAvance;
    }

    public function setMontantAvance(?float $montantAvance)
    {
        $this->montantAvance = $montantAvance;

        return $this;
    }

    public function getMontantAPAyer()
    {
        return $this->montantAPAyer;
    }

    public function setMontantAPAyer(?float $montantAPAyer)
    {
        $this->montantAPAyer = $montantAPAyer;

        return $this;
    }

    public function getMontantARecevoir()
    {
        return $this->montantARecevoir;
    }

    public function setMontantARecevoir(?float $montantARecevoir)
    {
        $this->montantARecevoir = $montantARecevoir;

        return $this;
    }


}

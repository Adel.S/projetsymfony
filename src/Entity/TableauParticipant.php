<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;


class TableauParticipant
{

    private $Participant;


    public function __construct()
    {
        $this->Participant = new ArrayCollection();
    }

    public function getParticipant()
    {
        return $this->Participant;
    }

    public function addParticipant(Participant $participant)
    {
        $this->Participant->add($participant);
    }

    public function removeParticipant(Participant $participant)
    {
        $this->Participant->removeElement($participant);

    }

   public function setParticipant($Participant)
    {
        $this->Participant = $Participant;

        return $this;
    }
}

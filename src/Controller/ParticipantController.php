<?php

namespace App\Controller;


use App\Entity\Participant;
use App\Entity\TableauParticipant;
use App\Form\TableauType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ParticipantController extends AbstractController
{
    /**
     * @Route("/", name="participant")
     */

    public function index(Request $request )
    {
        $tableauParticipant = new TableauParticipant();

        $participant = new Participant();

        $tableauParticipant->getParticipant()->add($participant);

        $form = $this->createForm(TableauType::class, $tableauParticipant);

        $form->handleRequest($request);

        $sommeTotal = array();

        $tabParticipant = array();

        $total = 0;

        $nbParticipant = 0;

        $sommeParParticipant = 0;

        $payeur = array();

        $receveur = array();

        $solution = array();

        $phrase= "";
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            foreach ($data->getParticipant() as $value) {
                array_push($sommeTotal, $value->getMontantAvance());
            }

            $total = array_sum($sommeTotal);

            $nbParticipant = count($data->getParticipant());
            if ($nbParticipant != 0) {
                $sommeParParticipant = $total / $nbParticipant;
                $sommeParParticipant = round($sommeParParticipant, 2);
            }

            foreach ($data->getParticipant() as $value) {

                if ($sommeParParticipant < $value->getMontantAvance()) {
                    $value->setMontantARecevoir($value->getMontantAvance() - $sommeParParticipant);

                } else if ($sommeParParticipant > $value->getMontantAvance()) {
                    $value->setMontantAPayer($value->getMontantAvance() - $sommeParParticipant);

                } else if ($sommeParParticipant == $value->getMontantAvance()) {
                    $value->setMontantAPayer(0);
                    $value->setMontantARecevoir(0);
                }

                $Participant = array(
                    'nom' => $value->getNom(),
                    "montantARecevoir" => $value->getMontantARecevoir(),
                    "montantAPayer" => ($value->getMontantAPayer() * -1)
                );

                array_push($tabParticipant, $Participant);
            }
            foreach ($tabParticipant as $value) {
                if ($value["montantAPayer"] != 0) {
                    array_push($payeur, $value);
                } else if ($value["montantARecevoir"] != 0) {
                    array_push($receveur, $value);
                }
            }
            foreach ($receveur as $key => $value) {

                foreach ($payeur as $key1 => $value1) {
                    while ($value["montantARecevoir"] != 0 && $value1["montantAPayer"] != 0) {
                        if ($value["montantARecevoir"] > $value1["montantAPayer"]) {

                            $receveur[$key]["montantARecevoir"] = $value["montantARecevoir"] - $value1["montantAPayer"];

                           // $value["montantARecevoir"] = $value["montantARecevoir"] - $value1["montantAPayer"];

                            $phrase = $value1["nom"]." doit donner ".$value1["montantAPayer"]." à ".$value["nom"];

                            $receveur[$key]["montantAPayer"] = $value1["montantAPayer"] = 0;

                        } else if ($value["montantARecevoir"] < $value1["montantAPayer"]) {

                            $payeur[$key1]["montantAPayer"] = $value1["montantAPayer"] - $value["montantARecevoir"];

                            $value1["montantAPayer"] =  $value1["montantAPayer"] - $value["montantARecevoir"];

                            $phrase = $value1["nom"]." doit donner ".$value["montantARecevoir"]." à ".$value["nom"];

                            $receveur[$key]["montantARecevoir"] = 0;


                        } else if ($value["montantARecevoir"] == $value1["montantAPayer"]) {
                            $value["montantARecevoir"] = 0;

                            $receveur[$key]["montantARecevoir"] = 0;

                            $phrase = $value1["nom"]." doit donner ".$value1["montantAPayer"]." à ".$value["nom"];
                        }
                        array_push($solution, $phrase);
                    }


                }

            }

        }


        return $this->render('participant/index.html.twig', [

            'form'=>$form->createView(),
            'tabParticipant'=>$tabParticipant,
            'totalSomme'=>$total,
            'nbParticipant'=>$nbParticipant,
            'sommeParParticipant'=>$sommeParParticipant,
            'solution'=>$solution


        ]);
    }
}
